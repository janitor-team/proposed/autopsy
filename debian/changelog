autopsy (2.24-5) unstable; urgency=medium

  * QA upload.
  * Ran wrap-and-sort -a.
  * debian/rules: migrated to reduced format. Consequently:
      - Created debian/autopsy.docs and debian/autopsy.manpages.
  * debian/autopsy.lintian-overrides: created.
  * debian/control:
      - Added VCS fields to use Salsa.
      - Reordered fields.
  * debian/copyright:
      - Updated License paragraphs.
      - Updated packaging copyright.
  * debian/gbp.conf: created to configure git-buildpackage.
  * debian/patches/05-find-perm.patch: added 'Forwarded: not-needed'.
  * debian/salsa-ci.yml: created to provide CI tests for Salsa.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Mon, 12 Oct 2020 22:18:51 +0000

autopsy (2.24-4) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Bumped Standards-Version to 4.5.0.
      - Set Rules-Requires-Root:no.
  * debian/copyright: added myself to copyright statement for debian/*.
  * debian/patches:
      - 06-fix-spelling-error-in-manpage.patch: Added to fix some spelling.
  * debian/upstream/metadata: Created.
  * debian/watch: added.

 -- Joao Paulo Lima de Oliveira <jlima.oliveira11@gmail.com>  Sat, 06 Jun 2020 13:21:58 -0300

autopsy (2.24-3) unstable; urgency=medium

  * QA upload.
  * debian/control:
      - Added Homepage field.
  * debian/copyright:
      - Update to format standard machine-readable.
      - Updated file.
  * debian/patches:
      - Disabled the 02-sleuthkit.patch because this patch broken the
        path the Sleuthkit commands: ils, icat and mactime.
        (LP: #1482976, LP: #1645312)

 -- Marcio de Souza Oliveira <m.desouza20@gmail.com>  Fri, 16 Jun 2017 04:09:53 +0000

autopsy (2.24-2) unstable; urgency=low

  [ Adam Borowski ]
  * QA upload.
  * Orphan the package. Closes: #809843

  [ Jari Aalto ]
  * Remove deprecated dpatch and upgrade to packaging format "3.0 quilt".
  * Update to Standards-Version to 3.9.3 and debhelper to 9.

 -- Adam Borowski <kilobyte@angband.pl>  Thu, 04 Aug 2016 08:11:08 +0200

autopsy (2.24-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Use "find -perm /x" instead of "find -perm +x". Closes: #803030

 -- Andreas Metzler <ametzler@debian.org>  Sun, 20 Dec 2015 14:02:36 +0100

autopsy (2.24-1) unstable; urgency=low

  * New upstream release

 -- Lorenzo Martignoni <martignlo@debian.org>  Sun, 20 Jun 2010 21:04:54 +0200

autopsy (2.23-2) unstable; urgency=low

  * Rebuilt package because prior version was improperly built (no diff file)
  * Updated to standard 3.8.4
  * Package is lintian clean

 -- Lorenzo Martignoni <martignlo@debian.org>  Mon, 22 Feb 2010 14:59:13 +0100

autopsy (2.23-1) unstable; urgency=low

  * New upstream release.

 -- Lorenzo Martignoni <martignlo@debian.org>  Mon, 22 Feb 2010 12:15:58 +0100

autopsy (2.21-2) unstable; urgency=low

  * Rebuilt package for unstable (Closes: #540514)
  * Updated to standard 3.8.3

 -- Lorenzo Martignoni <martignlo@debian.org>  Tue, 08 Dec 2009 10:30:17 +0100

autopsy (2.21-1) experimental; urgency=low

  * New upstream release (Closes: #464868)

 -- Lorenzo Martignoni <martignlo@debian.org>  Tue, 16 Jun 2009 22:46:38 +0200

autopsy (2.10-1) unstable; urgency=low

  * New upstream release (Closes: #464868)

 -- Lorenzo Martignoni <martignlo@debian.org>  Sun, 26 Apr 2009 17:22:17 +0200

autopsy (2.08-2.1) unstable; urgency=low

  * NMU
  * Applied patch to prevent broken Perl check, closes: #479935.
    Thanks to Peter Green <plugwash@p10link.net>.

 -- Martin A. Godisch <godisch@debian.org>  Sat, 19 Jul 2008 08:25:23 +0200

autopsy (2.08-2) unstable; urgency=low

  * Patched the code to point directly to ils-sleuthkit, icat-sleuthkit
    and mactime-sleuthkit instead of using original filenames (without
    -sleuthkit) which, due to the alternative system, could point to
    incompatible executables (Closes: #407011)

 -- Lorenzo Martignoni <martignlo@debian.org>  Sat, 20 Jan 2007 05:13:15 +0100

autopsy (2.08-1) unstable; urgency=low

  * New upstream release (Closes: #386412)

 -- Lorenzo Martignoni <martignlo@debian.org>  Tue, 17 Oct 2006 05:13:55 +0200

autopsy (2.06-2) unstable; urgency=low

  * Patched autopsy executable to add warning when it is not run as root
    (Closes: #344965)

 -- Lorenzo Martignoni <martignlo@debian.org>  Sat, 14 Jan 2006 15:42:09 +0100

autopsy (2.06-1) unstable; urgency=low

  * New upstream release
  * Updated package description

 -- Lorenzo Martignoni <martignlo@debian.org>  Sun, 23 Oct 2005 23:59:40 +0200

autopsy (2.05-1) unstable; urgency=high

  * New upstream release (Closes: #308990)

 -- Lorenzo Martignoni <martignlo@debian.org>  Thu, 19 May 2005 23:11:55 +0200

autopsy (2.04-3) unstable; urgency=low

  * Applied patch from Kenny Duffus to refles sstring executable Debian
    name (Closes: #304675)

 -- Lorenzo Martignoni <lorenzo.martignoni@poste.it>  Fri,  1 Apr 2005 00:00:33 +0200

autopsy (2.04-2) unstable; urgency=low

  * Applied patch from Kenny Duffus to fix sorter executable path

 -- Lorenzo Martignoni <lorenzo.martignoni@poste.it>  Wed, 30 Mar 2005 20:13:37 +0200

autopsy (2.04-1) unstable; urgency=low

  * New upstream release

 -- Lorenzo Martignoni <lorenzo.martignoni@poste.it>  Sun, 27 Mar 2005 14:23:09 +0200

autopsy (2.03-2) unstable; urgency=low

  * Patched the code to call 'datastat' instead 'dstat' to reflect changes
    introduced in the latest version of the Sleuthkit Debian package
    (patch provided by Kenny Duffus) (Closes: #291548)
  * Patched the code to use 'md5sum' and 'sha1sum'. Previous version of
    the patch was buggy (the new patch is provided by Kenny Duffus)
    (Closes: #291550)

 -- Lorenzo Martignoni <lorenzo.martignoni@poste.it>  Tue, 25 Jan 2005 23:27:03 +0100

autopsy (2.03-1) unstable; urgency=low

  * New upstream release

 -- Lorenzo Martignoni <lorenzo.martignoni@poste.it>  Fri, 17 Sep 2004 00:04:18 +0200

autopsy (2.02-1) unstable; urgency=low

  * New upstream release
  * Package is now built using dpatch

 -- Lorenzo Martignoni <lorenzo.martignoni@poste.it>  Tue, 03 Aug 2004 14:06:42 +0200

autopsy (1.75-1) unstable; urgency=low

  * New upstream release

 -- Lorenzo Martignoni <lorenzo.martignoni@poste.it>  Fri, 28 Nov 2003 19:32:15 +0100

autopsy (1.71-2) unstable; urgency=low

  * Package taken while Lorenzo is waiting to become an official Debian
    mantainer

 -- Mattia Monga <monga@debian.org>  Thu, 17 Apr 2003 22:33:36 +0200

autopsy (1.71-1) unstable; urgency=low

  * New upstream release

 -- Lorenzo Martignoni <lorenzo.martignoni@milug.org>  Tue, 15 Apr 2003 19:09:34 +0200

autopsy (1.70-1) unstable; urgency=low

  * Initial Release.

 -- Lorenzo Martignoni <lorenzo.martignoni@milug.org>  Tue,  1 Apr 2003 18:01:22 +0200
